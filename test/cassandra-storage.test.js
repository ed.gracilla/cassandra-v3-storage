/* global describe, it, before, after */
'use strict'

const amqp = require('amqplib')
const should = require('should')
const moment = require('moment')

const ID = Date.now()

let _app = null
let _conn = null
let _channel = null

/*let conf = {
  host: '127.0.0.1',
  port: '9042',
  user: 'cassandra',
  password: 'f312e8989361c1cdd4f04562d53742ea',
  keyspace: 'reekoh',
  schema: {
    fields: {
      id: {
        type: 'uuid',
        default: {'$db_function': 'uuid()'}
      },
      data_id: {
        type: 'bigint'
      },
      co2: {type: 'text'},
      temp: 'int',
      quality: 'float',
      reading_time: 'timestamp',
      metadata: {
        type: 'map',
        typeDef: '<varchar, text>'
      },
      list: {
        type: 'list',
        typeDef: '<varchar>'
      },
      set: {
        type: 'set',
        typeDef: '<varchar>'
      },
      random_data: 'varchar',
      is_normal: 'boolean',
      created: {
        type: 'timestamp',
        default: {
          '$db_function': 'toTimestamp(now())'
        }
      }
    },
    key: [['id'], 'created'],
    clustering_order: {'created': 'desc'},
    table_name: 'home_control'
  }
}*/

let record = {
  data_id: ID,
  co2: '11%',
  temp: 23,
  quality: 11.25,
  reading_time: '2015-11-27T11:04:13.539Z',
  metadata: {
    metadata_json: 'reekoh metadata json'
  },
  list: ['value1', 'value1', 'value2'],
  set: ['value1', 'value2'],
  random_data: 'abcdefg',
  is_normal: true
}

describe('Cassandra (v3) Storage', function () {
  // process.env.ACCOUNT = 'demo.account'
  // process.env.INPUT_PIPE = 'demo.pipe.storage'
  // process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
  // process.env.CONFIG = '{"host":"127.0.0.1","port":"9042","user":"cassandra","password":"","keyspace":"reekoh","schema":{"fields":{"id":{"type":"uuid","default":{"$db_function":"uuid()"}},"data_id":{"type":"bigint"},"co2":{"type":"text"},"temp":"int","quality":"float","reading_time":"timestamp","metadata":{"type":"map","typeDef":"<varchar, text>"},"list":{"type":"list","typeDef":"<varchar>"},"set":{"type":"set","typeDef":"<varchar>"},"random_data":"varchar","is_normal":"boolean","created":{"type":"timestamp","default":{"$db_function":"toTimestamp(now())"}}},"key":[["id"],"created"],"clustering_order":{"created":"desc"},"table_name":"home_control"}}'
  
  let BROKER = process.env.BROKER
  let CONFIG = process.env.CONFIG
  let INPUT_PIPE = process.env.INPUT_PIPE

  before('init', () => {
    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate child process', function () {
    _conn.close()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', function () {
    it('should process the data', function (done) {
      this.timeout(10000)

      _channel.sendToQueue(INPUT_PIPE, new Buffer(JSON.stringify(record)))
      _app.on('processed', done)
    })
  })

  describe('#data', function () {
    it('should have inserted the data', function (done) {
      this.timeout(10000)

      let conf = JSON.parse(CONFIG)
      let Cassandra = require('express-cassandra')

      let cassandra = Cassandra.createClient({
        clientOptions: {
          contactPoints: `${conf.host}`.replace(/\s/g, '').split(','),
          keyspace: conf.keyspace,
          protocolOptions: {
            port: conf.port
          },
          authProvider: new Cassandra.driver.auth.DsePlainTextAuthProvider(conf.user, conf.password),
          queryOptions: {
            consistency: Cassandra.consistencies.one
          }
        },
        ormOptions: {
          defaultReplicationStrategy: Object.assign({
            class: 'SimpleStrategy',
            replication_factor: 1
          }),
          migration: 'safe',
          createKeyspace: true
        }
      })

      cassandra.connect(function (connectionError) {
        should.ifError(connectionError)

        // ci weird err fix. '$db_function' returned empty
        if (!conf.schema.fields.id.default.$db_function) {
          conf.schema.fields.id.default = {'$db_function': 'uuid()'}
        }
        if (!conf.schema.fields.created.default.$db_function) {
          conf.schema.fields.created.default = {'$db_function': 'toTimestamp(now())'}
        }
        
        let Data = cassandra.loadSchema('Data', conf.schema, function (modelError) {
          should.ifError(modelError)

          Data.findOne({data_id: cassandra.datatypes.Long.fromString(`${ID}`)}, {allow_filtering: true}, function (findError, data) {
            should.ifError(findError)
            should.exist(data)

            should.equal(record.co2, data.co2, 'Data validation failed. Field: co2')
            should.equal(record.temp, data.temp, 'Data validation failed. Field: temp')
            should.equal(record.quality, data.quality, 'Data validation failed. Field: quality')
            should.equal(record.random_data, data.random_data, 'Data validation failed. Field: random_data')
            should.equal(moment(record.reading_time).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'), moment(data.reading_time).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'), 'Data validation failed. Field: reading_time')
            should.equal(JSON.stringify(record.metadata), JSON.stringify(data.metadata), 'Data validation failed. Field: metadata')
            should.equal(record.is_normal, data.is_normal, 'Data validation failed. Field: is_normal')
            done()
          })
        })
      })
    })
  })
})
