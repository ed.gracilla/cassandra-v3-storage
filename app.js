'use strict'

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Storage()

const async = require('async')
const moment = require('moment')
const isNil = require('lodash.isnil')
const isEmpty = require('lodash.isempty')
const isArray = require('lodash.isarray')
const isNumber = require('lodash.isnumber')
const contains = require('lodash.contains')
const isString = require('lodash.isstring')
const isPlainObject = require('lodash.isplainobject')

const Cassandra = require('express-cassandra')

let cassandra = null
let schema = null
let Data = null

let insertData = (processedData, callback) => {
  let data = new Data(processedData)
  data.save(callback)
}

let processData = (data, callback) => {
  let fields = schema.fields
  let processedData = {}

  async.forEachOf(fields, (field, key, done) => {
    try {
      let dataType = ''

      if (isString(fields[key])) {
        dataType = `${fields[key]}`.toLowerCase()
      } else if (isPlainObject(fields[key]) && fields[key].type) {
        dataType = `${fields[key].type}`.toLowerCase()
      }

      // bigint
      if (!isNil(fields[key]) && (dataType === 'bigint' || dataType === 'bigint') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.Long.fromString(`${data[key]}`)

      // blob
      } else if (!isNil(fields[key]) && (dataType === 'blob' || dataType === 'blob') && Buffer.isBuffer(data[key])) {
        processedData[key] = data[key]
      } else if (!isNil(fields[key]) && (dataType === 'blob' || dataType === 'blob') && !isNil(data[key])) {
        processedData[key] = new Buffer(`${data[key]}`, 'base64')

      // boolean
      } else if (!isNil(fields[key]) && (dataType === 'boolean' || dataType === 'boolean')) {
        processedData[key] = !!(data[key] || !isNil(data[key]))

      // counter
      } else if (!isNil(fields[key]) && (dataType === 'counter' || dataType === 'counter') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.Long.fromString(`${data[key]}`)

      // date
      } else if (!isNil(fields[key]) && (dataType === 'date' || dataType === 'date') && Buffer.isBuffer(data[key])) {
        processedData[key] = cassandra.datatypes.LocalDate.fromBuffer(moment(`${data[key]}`).toDate())
      } else if (!isNil(fields[key]) && (dataType === 'date' || dataType === 'date') && !isNil(data[key]) && moment(`${data[key]}`).isValid()) { processedData[key] = cassandra.datatypes.LocalDate.fromDate(moment(`${data[key]}`).toDate()) } else if (!isNil(fields[key]) && (dataType === 'date' || dataType === 'date') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.LocalDate.fromString(`${data[key]}`)

      // decimal
      } else if (!isNil(fields[key]) && (dataType === 'decimal' || dataType === 'decimal') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.BigDecimal.fromString(`${data[key]}`)

      // double, float, int, smallint, tinyint
      } else if (!isNil(fields[key]) && (contains(['double', 'float', 'int', 'smallint', 'tinyint'], dataType) || contains(['double', 'float', 'int', 'smallint', 'tinyint'], dataType)) && !isNil(data[key])) {
        processedData[key] = Number(`${data[key]}`)

      // inet
      } else if (!isNil(fields[key]) && (dataType === 'inet' || dataType === 'inet') && Buffer.isBuffer(data[key])) {
        processedData[key] = new cassandra.datatypes.InetAddress(data[key])
      } else if (!isNil(fields[key]) && (dataType === 'inet' || dataType === 'inet') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.InetAddress.fromString(`${data[key]}`)

      // list, set
      } else if (!isNil(fields[key]) && (contains(['list', 'set'], dataType) || contains(['list', 'set'], dataType)) && isArray(data[key])) {
        processedData[key] = data[key]
      } else if (!isNil(fields[key]) && (contains(['list', 'set'], dataType) || contains(['list', 'set'], dataType)) && !isNil(data[key])) {
        processedData[key] = JSON.parse(`${data[key]}`)

      // map
      } else if (!isNil(fields[key]) && (dataType === 'map' || dataType === 'map') && isPlainObject(data[key])) {
        processedData[key] = data[key]
      } else if (!isNil(fields[key]) && (dataType === 'map' || dataType === 'map') && !isNil(data[key])) {
        processedData[key] = JSON.parse(`${data[key]}`)

      // time
      } else if (!isNil(fields[key]) && (dataType === 'time' || dataType === 'time') && Buffer.isBuffer(data[key])) { processedData[key] = cassandra.datatypes.LocalTime.fromBuffer(moment(`${data[key]}`).toDate()) } else if (!isNil(fields[key]) && (dataType === 'time' || dataType === 'time') && isNumber(data[key])) {
        processedData[key] = cassandra.datatypes.LocalTime.fromMilliseconds(data[key])
      } else if (!isNil(fields[key]) && (dataType === 'time' || dataType === 'time') && !isNil(data[key]) && moment(`${data[key]}`).isValid()) { processedData[key] = cassandra.datatypes.LocalTime.fromDate(moment(`${data[key]}`).toDate()) } else if (!isNil(fields[key]) && (dataType === 'time' || dataType === 'time') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.LocalTime.fromString(`${data[key]}`)

      // timestamp
      } else if (!isNil(fields[key]) && (dataType === 'timestamp' || dataType === 'timestamp') && !isNil(data[key]) && moment(`${data[key]}`).isValid()) {
        processedData[key] = moment(`${data[key]}`).toDate()

      // timeuuid
      } else if (!isNil(fields[key]) && (dataType === 'timeuuid' || dataType === 'timeuuid') && !isNil(data[key]) && moment(`${data[key]}`).isValid()) { processedData[key] = cassandra.datatypes.TimeUuid.fromDate(moment(`${data[key]}`).toDate()) } else if (!isNil(fields[key]) && (dataType === 'timeuuid' || dataType === 'timeuuid') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.TimeUuid.fromString(`${data[key]}`)

      // tuple
      } else if (!isNil(fields[key]) && (dataType === 'tuple' || dataType === 'tuple') && isArray(data[key])) {
        processedData[key] = cassandra.datatypes.Tuple.fromArray(data[key])

      // uuid
      } else if (!isNil(fields[key]) && (dataType === 'uuid' || dataType === 'uuid') && Buffer.isBuffer(data[key])) {
        processedData[key] = new cassandra.datatypes.Uuid(data[key])
      } else if (!isNil(fields[key]) && (dataType === 'uuid' || dataType === 'uuid') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.Uuid.fromString(`${data[key]}`)

      // varint
      } else if (!isNil(fields[key]) && (dataType === 'varint' || dataType === 'varint') && Buffer.isBuffer(data[key])) {
        processedData[key] = cassandra.datatypes.Integer.fromBuffer(moment(`${data[key]}`).toDate())
      } else if (!isNil(fields[key]) && (dataType === 'varint' || dataType === 'varint') && !isNil(data[key])) {
        processedData[key] = cassandra.datatypes.Integer.fromString(`${data[key]}`)

      // ascii, text, varchar and others
      } else {
        processedData[key] = (!isNil(data[key])) ? `${data[key]}` : undefined
      }

      done()
    } catch (ex) {
      done()
    }
  }, () => {
    callback(null, processedData)
  })
}

plugin.on('data', (data) => {
  if (isPlainObject(data)) {
    processData(data, (error, processedData) => {
      if (error) return plugin.logException(error)

      insertData(processedData, (error) => {
        if (error) return plugin.logException(error)

        plugin.emit('processed')
        plugin.log(JSON.stringify({
          title: 'Record Successfully inserted to Cassandra Database.',
          data: processedData
        }))
      })
    })
  } else if (isArray(data)) {
    async.each(data, (datum) => {
      processData(datum, (error, processedData) => {
        if (error) return plugin.logException(error)

        insertData(processedData, (error) => {
          if (!error) {
            plugin.log(JSON.stringify({
              title: 'Record Successfully inserted to Cassandra Database.',
              data: processedData
            }))
          } else {
            plugin.logException(error)
          }
        })
      })
    })
  } else {
    plugin.logException(new Error(`Invalid data received. Data must be a valid Array/JSON Object or a collection of objects. Data: ${data}`))
  }
})

plugin.once('ready', () => {
  let options = plugin.config
  let host = `${options.host}`.replace(/\s/g, '').split(',')

  schema = options.schema
  console.log('aaa', typeof schema, schema.fields.id.default)

  // ci weird err fix. '$db_function' returned empty

  if (!schema.fields.id.default.$db_function) {
    schema.fields.id.default = {'$db_function': 'uuid()'}
  }
  if (!schema.fields.created.default.$db_function) {
    schema.fields.created.default = {'$db_function': 'toTimestamp(now())'}
  }

  let clientOptions = {
    contactPoints: host,
    keyspace: options.keyspace,
    protocolOptions: {
      port: options.port || 9042
    }
  }

  let ormOptions = {
    defaultReplicationStrategy: {
      class: 'SimpleStrategy',
      replication_factor: 1
    },
    migration: 'safe',
    createKeyspace: true
  }

  if (!isEmpty(options.replicationConfig)) {
    try {
      ormOptions.defaultReplicationStrategy = Object.assign({
        class: 'NetworkTopologyStrategy'
      }, JSON.parse(options.replicationConfig))
    } catch (ex) {
      plugin.logException(new Error(`Invalid replication config provided. Replication config must be a valid JSON String.`))

      return setTimeout(() => {
        process.exit(1)
      }, 5000)
    }
  } else if (!isNil(options.replication_factor)) {
    ormOptions.defaultReplicationStrategy.replication_factor = parseInt(options.replication_factor)
  }

  if (options.port) {
    clientOptions.protocolOptions = {port: options.port}
  }

  cassandra = Cassandra.createClient({
    clientOptions: Object.assign(clientOptions, {
      authProvider: (!isEmpty(options.user) || !isEmpty(options.password)) ? new Cassandra.driver.auth.DsePlainTextAuthProvider(options.user, options.password) : undefined,
      queryOptions: {
        consistency: Cassandra.consistencies.one
      }
    }),
    ormOptions: ormOptions
  })

  cassandra.connect((connectionError) => {
    if (connectionError) return plugin.logException(connectionError)

    Data = cassandra.loadSchema('Data', schema, (modelError) => {
      if (modelError) return plugin.logException(modelError)

      plugin.log('Cassandra Storage plugin ready.')
      plugin.emit('init')
    })
  })
})

module.exports = plugin
